# Wiirbelbot commands

## Mod commands

- timeban

    ###### Beispiel
    
    ```
    timeban "Wiirbeltesti#3485" 0 2018-3-4-17-15-0
    ```

    `"Wiirbeltesti#3485` - Userid
    
    `0` - Anzahl Tage zu löschender Nachricht des zu bannenden Users

    `2018-3-4-17-15-0` - Enddatum des bannes in
    **`Jahr`-`Monat`-`Tag`-`Stunde`-`Minute`-`Sekunde`** 

- deleteById

    ###### Beispiel

    ```
    moderator.delete.byID lauchlinge 420275937347764224
    ```

    `lauchlinge` - Der Name des Textchannels, in dem die Nachricht ist.

    `420275937347764224` Die ID der zu löschenden Nachricht (auslesbar per Entwicklermode von Discord oder, vom Bot ausgespuckt, wenn eine Nachricht eines der Worte der Blacklist enthielt)